The tests here check some functions on toptal.com website.

Tests are written in python, using selene with chrome opened in docker container.


UI test cases description

`test_main_page`
* open toptal.com
* verify that main controls are present - Login, Hire, Talent apply buttons

`test_hire_quiz_open`
* Open toptal.com/hire page
* select Developers role
* verify that quiz is opened and needed tet is shown

`test_login_blank`
* open Login page
* do not fill any fields
* click on Login button
* verify that url didn't change and user is still on login page