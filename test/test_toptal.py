from selene.support.conditions import have
from selene import browser
from selene.support.jquery_style_selectors import s

from helpers.utils import get_chrome
from po.main import ToptalMainPage
from po.hire import ToptalHireTalentPage
from po.login import ToptalLoginPage


class TestTopTal:

    def setup_method(self):
        self.chrome = get_chrome()
        browser.set_driver(self.chrome.get_driver())

    def teardown_method(self):
        browser.close()
        self.chrome.stop()

    def test_main_page(self):
        main_page = ToptalMainPage().open()
        assert main_page.login_button.is_displayed()
        assert main_page.talent_apply_button.is_displayed()
        assert main_page.hire_talent_button.is_displayed()

    def test_hire_quiz_open(self):
        hire_page = ToptalHireTalentPage().open()
        hire_page.select_role_proceed("developers")
        assert s("[class='step-header']").should(
            have.text("What is the size of your company?"))
        browser.should(have.url("https://www.toptal.com/hire/quiz/company-size"))

    def test_login_blank(self):
        login_page = ToptalLoginPage().open()
        assert login_page.email_field.is_displayed()
        login_page.click_login()
        browser.should(have.url(login_page.url))
