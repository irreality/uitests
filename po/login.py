from selene.browser import open_url
from selene.support.jquery_style_selectors import s


class ToptalLoginPage(object):

    url = "https://www.toptal.com/users/login"
    login_button = s("[name='commit']")
    email_field = s("[id='user_email']")

    def open(self):
        open_url(self.url)
        return self

    def click_login(self):
        self.login_button.click()
        return self
