from selene.browser import open_url
from selene.support.jquery_style_selectors import s

from po.hire import ToptalHireTalentPage
from po.login import ToptalLoginPage


class ToptalMainPage(object):

    login_button = s('[href="/users/login"]')
    talent_apply_button = s('[href="/talent/apply"]')
    login_button = s('[href="/users/login"]')
    hire_talent_button = s('[data-role="companies_apply_button"]')

    def open(self):
        open_url("http://toptal.com/")
        return self

    def hire_talent(self):
        self.hire_talent_button.click()
        return ToptalHireTalentPage()

    def open_login_page(self):
        self.login_button.click()
        return ToptalLoginPage()
