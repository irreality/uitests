from selene.browser import open_url
from selene.support.jquery_style_selectors import s, ss
from selene.support.conditions import have


class ToptalHireTalentPage(object):

    url = "https://www.toptal.com/hire?interested_in=developers"
    get_started_button = s("[id=save_new_company_create_lead]")
    type_select = s("[id='lead_interested_in']")
    email_field = s("[name='email']")
    company_name_field = s("[name='company_name']")
    name_field = s("[name='contact_name']")
    skype_field = s("[name='skype']")

    def open(self):
        open_url(self.url)
        return self

    def select_role_proceed(self, role):
        s("[value='{}']".format(role)).click()
        self.click_get_started()

    def click_get_started(self):
        self.get_started_button.click()
        return self

    def fill_form(self, type_selection, email, company, name, skype):
        self.type_select.all("option").element_by(
            have.value(type_selection)).click()
        self.email_field.set(email)
        self.company_name_field.set(company)
        self.name_field.set(name)
        self.skype_field.set(skype)
        self.click_get_started()

