How to run tests:

1. Install pipenv
  * for ubuntu it is like: sudo pip install pipenv

2. In the current dir install dependecies:
  `pipenv install`

3. Activate virtual env:
  `pipenv shell`

4. Run tests:
  `env PYTHONPATH=. py.test --alluredir=report --disable-pytest-warnings`