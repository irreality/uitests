from testcontainers.selenium import BrowserWebDriverContainer
from selenium.webdriver import DesiredCapabilities


def get_chrome():
    chrome = BrowserWebDriverContainer(DesiredCapabilities.CHROME)
    chrome.start()
    return chrome